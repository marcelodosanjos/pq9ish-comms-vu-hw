# PQ9ISH V/U COMMS/OBC Hardware

A pocketcube format main board around STM32 with AX5043 transceiver utlizing the modified PQ9 standard.

## Storage

## Comms
AX5043 COMMS Transceiver with PA and dedicated RF lines for RX and TX

## Instructions

Clone the repository with --recursive to make sure lsf-kicad-lib submodule is initialiazed.

## License

Licensed under the [CERN OHLv1.2](LICENSE) 2018 Libre Space Foundation.
